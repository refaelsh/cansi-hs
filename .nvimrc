nnoremap <F5> :wa <bar> :set makeprg=cabal\ build <bar> :compiler ghc <bar> :make <CR>
nnoremap <F7> :wa <bar> :!cabal new-test --test-show-details=streaming <CR>
" nnoremap <F10> :silent !kitty -e cabal run& <CR> :redraw<CR>        
