module Main (main) where

import MyLib (Color (..), Slice (..), categorize {- , deliminatedByEscape, textCodePairs -})
import Test.HUnit (assertEqual)
import Text.EscapeArtist

main :: IO ()
main = do
  -- print $ textCodePairs "Hello, \x1b[31;4mworld\x1b[0m!"
  -- print $ categorize "Hello, \x1b[31;4mworld\x1b[0m!"

  assertEqual
    "Bold (bright) green hello"
    (categorize "\x1b[1;32mHello\x1b[0m")
    [ Slice "Hello" (Color 0xAAFF00 "bright_green")
    ]

  assertEqual
    "Green hello"
    (categorize $ escToString $ FgGreen "Hello")
    [ Slice "Hello" (Color 0x00FF00 "green")
    ]

  assertEqual
    "Hello world whatever"
    (categorize (escToString (FgGreen "Hello " <> FgYellow "world ") ++ "whatever"))
    [ Slice "Hello " (Color 0x00FF00 "green"),
      Slice "world " (Color 0xFFFF00 "yellow"),
      Slice "whatever" (Color 0xFFFFFF "white")
    ]

  assertEqual
    "Colored 42"
    (categorize $ escToString $ FgBlue "4" <> FgWhite "2")
    [ Slice "4" (Color 0x0000FF "blue"),
      Slice "2" (Color 0xFFFFFF "white")
    ]

  assertEqual
    "Red color test"
    (categorize "Hello, \x1b[31mworld\x1b[0m!")
    [ Slice "Hello, " (Color 0xFFFFFF "white"),
      Slice "world" (Color 0xFF0000 "red"),
      Slice "!" (Color 0xFFFFFF "white")
    ]

  assertEqual
    "Red color from the beginning"
    (categorize "\x1b[31mworld\x1b[0m!")
    [ Slice "world" (Color 0xFF0000 "red"),
      Slice "!" (Color 0xFFFFFF "white")
    ]

  assertEqual
    "No escape sequences"
    (categorize "test")
    [ Slice "test" (Color 0xFFFFFF "white")
    ]

  assertEqual
    "Empty sequence"
    (categorize "\x1b[;mtest")
    [ Slice "test" (Color 0xFFFFFF "white")
    ]

  assertEqual
    "Empty text"
    (categorize "\x1b[;mtest\x1b[;m\x1b[;m")
    [ Slice "test" (Color 0xFFFFFF "white")
    ]
