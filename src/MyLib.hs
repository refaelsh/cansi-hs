module MyLib (Slice (..), Color (..), categorize {- , deliminatedByEscape, textCodePairs -}) where

import Data.Char (isAlpha)
import Data.List (findIndex)
import Data.List.Split
import Data.Maybe (fromJust)

categorize :: [Char] -> [Slice]
categorize codedText =
  map
    (\pair -> Slice (snd pair) (colorFromCode (fst pair)))
    (textCodePairs codedText)

data Slice = Slice
  { text :: String,
    color :: Color
  }
  deriving (Show, Eq)

data Color = Color
  { rgb :: Int,
    string :: String
  }
  deriving (Show, Eq)

colorFromCode :: [Char] -> Color
colorFromCode [] = Color 0xFFFFFF "white"
colorFromCode code = case drop 2 (init code) of
  "0" -> Color 0xFFFFFF "white"
  "30" -> Color 0x000000 "black"
  "31" -> Color 0xFF0000 "red"
  "32" -> Color 0x00FF00 "green"
  "33" -> Color 0xFFFF00 "yellow"
  "34" -> Color 0x0000FF "blue"
  "35" -> Color 0xFF00FF "magenta"
  "36" -> Color 0x00FFFF "cyan"
  "37" -> Color 0xFFFFFF "white"
  "1;30" -> Color 0x222024 "bright_black"
  "1;31" -> Color 0xEE4B2B "bright_red"
  "1;32" -> Color 0xAAFF00 "bright_green"
  "1;33" -> Color 0xFFFF00 "bright_yellow"
  "1;34" -> Color 0x004EFF "bright_blue"
  "1;35" -> Color 0xFF00CD "bright_magenta"
  "1;36" -> Color 0x41FDFE "bright_cyan"
  "1;37" -> Color 0xFDFEFF "bright_white"
  -- From here: https://en.wikipedia.org/wiki/ANSI_escape_code.
  -- 39 Default foreground color Implementation defined (according to standard)
  "39" -> Color 0xFFFFFF "white"
  "40" -> Color 0x000000 "black"
  "41" -> Color 0xFF0000 "red"
  "42" -> Color 0x00FF00 "green"
  "43" -> Color 0xFFFF00 "yellow"
  "44" -> Color 0x0000FF "blue"
  "45" -> Color 0xFF00FF "magenta"
  "46" -> Color 0x00FFFF "cyan"
  "47" -> Color 0xFFFFFF "white"
  "90" -> Color 0x222024 "bright_black"
  "91" -> Color 0xEE4B2B "bright_red"
  "92" -> Color 0xAAFF00 "bright_green"
  "93" -> Color 0xFFFF00 "bright_yellow"
  "94" -> Color 0x004EFF "bright_blue"
  "95" -> Color 0xFF00CD "bright_magenta"
  "96" -> Color 0x41FDFE "bright_cyan"
  "97" -> Color 0xFDFEFF "bright_white"
  "100" -> Color 0x222024 "bright_black"
  "101" -> Color 0xEE4B2B "bright_red"
  "102" -> Color 0xAAFF00 "bright_green"
  "103" -> Color 0xFFFF00 "bright_yellow"
  "104" -> Color 0x004EFF "bright_blue"
  "105" -> Color 0xFF00CD "bright_magenta"
  "106" -> Color 0x41FDFE "bright_cyan"
  "107" -> Color 0xFDFEFF "bright_white"
  _ -> Color 0xFFFFFF "white"

textCodePairs :: [Char] -> [([Char], [Char])]
textCodePairs codedText =
  filter
    (not . null . snd)
    ( map
        ( \str ->
            if '\x1b' `elem` str
              then splitAt (fromJust (findIndex isAlpha str) + 1) str
              else ("", str)
        )
        (deliminatedByEscape codedText)
    )

deliminatedByEscape :: [Char] -> [[Char]]
deliminatedByEscape = split (keepDelimsL $ oneOf ['\x1b'])
