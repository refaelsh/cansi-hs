### Cansi-hs

`Cansi-hs` will parse text with ANSI escape sequences in it and return a deconstructed text with metadata around the coloring and styling.
`Cansi-hs` is only concerned with CSI sequences, particularly the SGR parameters.
`Cansi-hs` will not construct escaped text, there are other libraries for that.

### Status

1. No Unicode support yet.
1. No styling support yet (e.g., underline, bold, italic and etc).
1. Supports the bright colors.
1. It is usable, I use it :-)

### Inspiration
1. I could not find such a library written in Haskell.
1. This rust library (they say "crate" in Rustatian): [cansi](https://github.com/kurtlawrence/cansi).

### Development
Please feel free to open an issue or a pull request.
